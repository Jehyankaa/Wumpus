package graphDisplay;

import org.graphstream.graph.Graph;
import org.graphstream.graph.Node;
import org.graphstream.graph.implementations.SingleGraph;

/**
 * Graphical representation of the map (for every agent)
 * @author Jehyanka
 *
 */
public class GraphDisplayer {
	
	public final static String title="someTitle";
	private final Graph graph= new SingleGraph(title);
	
	public GraphDisplayer() {
		init();
	}

	public void init() {
		//color of a node according to its type
		String defaultNodeStyle= "node {"+"fill-color: black;"+" size-mode:fit;text-alignment:under; text-size:14;text-color:white;text-background-mode:rounded-box;text-background-color:black;}";
		String nodeStyle_open= "node.open {"+"fill-color: blue;"+"}";
		String nodeStyle_close= "node.close {"+"fill-color: red;"+"}";
		String nodeStyle=defaultNodeStyle+nodeStyle_open+nodeStyle_close;
		System.setProperty("org.graphstream.ui.renderer", "org.graphstream.ui.j2dviewer.J2DGraphRenderer");
		
		graph.setAttribute("ui.stylesheet",nodeStyle);
		graph.display();
	}
	
	//Add just one node without neighbours
	public void addOpenNode(mas.data.Node n){
		graph.addNode(n.getId());
		Node node= graph.getNode(n.getId());
		node.addAttribute("ui.label", n.getId());	
		node.setAttribute("ui.class", "open");
	}
	
	//add a node and his neighbours
	public void addClosedNode(mas.data.Node n){
		
		boolean unknownNode=(graph.getNode(n.getId())==null);
		if (unknownNode)
			graph.addNode(n.getId());
		Node node= graph.getNode(n.getId());
		if (unknownNode)
			node.addAttribute("ui.label", n.getId());	
		node.setAttribute("ui.class", "close");

		for(String n1:n.getNeighbours()) {
			unknownNode=(graph.getNode(n1)==null);
			if(unknownNode)
				graph.addNode(n1);
			Node nodeT=  graph.getNode(n1);
			if (unknownNode) {
				nodeT.addAttribute("ui.label", n1);	
				nodeT.setAttribute("ui.class", "open");
			}
			if(!node.hasEdgeToward(nodeT) && !nodeT.hasEdgeToward(node))
				graph.addEdge(n.getId()+n1, n.getId(), n1);
		}		
	}
	
	

}
