package mas.behaviours.receive;

import java.util.ArrayList;
import java.util.HashMap;

import jade.core.AID;
import jade.core.Agent;
import jade.core.behaviours.SimpleBehaviour;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.MessageTemplate;
import jade.lang.acl.UnreadableException;
import mas.agents.BasicAgent;
import mas.data.Node;
import mas.data.NodeDetails;

/**
 * Receives new updates about node - quantity of diamonds/treasure in each node
 * @author Jehyanka
 *
 */
public class ReceiveNodesBehaviour extends SimpleBehaviour{

	private static final long serialVersionUID = 9088209402507795289L;

	private boolean finished=false;
	
	public ReceiveNodesBehaviour(final Agent myagent) {
		super(myagent);
		
	}


	public void action() {
		BasicAgent agent= (BasicAgent)this.myAgent;

		//1) receive the hello messages
		final MessageTemplate msgTemplate = MessageTemplate.MatchPerformative(ACLMessage.INFORM_REF) ;
		final ACLMessage msg = this.myAgent.receive(msgTemplate);

		if (msg != null) {
			//System.out.println(this.myAgent.getLocalName()+"<----New Nodes Message received from "+msg.getSender().getLocalName());
			try {
				@SuppressWarnings("unchecked")
				ArrayList<NodeDetails> msgContent = (ArrayList<NodeDetails>)msg.getContentObject();
				
				NodeDetails temp;
				for(NodeDetails nd:msgContent) {
					//TODO Gérer les horloges (si mon info est plus récente je prend pas sinon je met à jour)
					temp=agent.getNodeDetailsById(nd.getId());
					if(temp==null)
						agent.addNodeDetails(nd);
					else
						if (temp.getDate()<nd.getDate()) {
							agent.removeNodeDetails(temp);					
							agent.addNodeDetails(nd);
						}
					
				}
				
				
				

			} catch (UnreadableException e) {
				e.printStackTrace();
			}

			
		}
	}


	@Override
	public boolean done() {
		return finished;
	}

}
