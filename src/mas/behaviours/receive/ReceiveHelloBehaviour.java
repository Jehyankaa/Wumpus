package mas.behaviours.receive;

import java.util.ArrayList;
import java.util.HashMap;

import jade.core.AID;
import jade.core.Agent;
import jade.core.behaviours.SimpleBehaviour;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.MessageTemplate;
import jade.lang.acl.UnreadableException;
import mas.agents.BasicAgent;
import mas.data.CollectorDetails;

/**
 * Receives hellos from everyone else. 
 * Note : the Hello from the SILO is special and can (and will be) recognized
 * @author Jehyanka
 *
 */
public class ReceiveHelloBehaviour extends SimpleBehaviour{

	private static final long serialVersionUID = 9088209402507795289L;

	private boolean finished=false;
	
	public ReceiveHelloBehaviour(final Agent myagent) {
		super(myagent);
		
	}


	public void action() {
		BasicAgent agent= (BasicAgent)this.myAgent;

		//1) receive the hello messages
		final MessageTemplate msgTemplate = MessageTemplate.MatchPerformative(ACLMessage.PROPAGATE) ;
		final ACLMessage msg = this.myAgent.receive(msgTemplate);

		if (msg != null) {

			//String hisPosition=(msg.getContent().split("at"))[1].trim();

			if(!agent.isEveryoneElseFinished()) {
				//System.out.println(agent.getLocalName()+" received "+msg.getSender().getLocalName()+"'s position which is : " + hisPosition);
				ArrayList<AID> finishedAgents=agent.getFinishedAgents();
				if (!finishedAgents.contains(msg.getSender()))
					finishedAgents.add(msg.getSender());
				agent.setFinishedAgents(finishedAgents);
				agent.setEveryoneElseFinished();
			}
			
			//then it's the silo
			if(msg.getContent().contains("Hello")) {
				if(agent.isCollector()) {
					System.out.println(agent.getLocalName()+" found the SILO and empty his backpack");
				
					agent.emptyMyBackPack(msg.getSender().getLocalName());
					agent.addMyDetails();
				}
				//agent.emptyMyBackPack(msg.getSender().getName());
				

			}

			else {
				
				try {
					@SuppressWarnings("unchecked")
					ArrayList<CollectorDetails> msgContent = (ArrayList<CollectorDetails>)msg.getContentObject();
					((BasicAgent)this.myAgent).updateCollectorDetails(msgContent);
					/*if(agent.getType().equals(BasicAgent.agentType.SILO)) {
						System.out.println("Le silo recoit :\n");
						for(CollectorDetails cd : msgContent) {
							System.out.println("L'"+cd.getAid().getLocalName()+" est en "+cd.getPosition());
						}
					}*/
				} catch (UnreadableException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
				


			
		}
	}


	@Override
	public boolean done() {
		return finished;
	}

}
