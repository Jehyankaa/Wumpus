package mas.behaviours.receive;

import jade.core.AID;
import jade.core.Agent;
import jade.core.behaviours.SimpleBehaviour;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.MessageTemplate;
import jade.lang.acl.UnreadableException;
import mas.agents.BasicAgent;
import mas.behaviours.SayHello;
import mas.behaviours.send.SendMapDataBehaviour;
import mas.data.Node;

import java.util.ArrayList;

/**
 *Receives map updates and map requests (only while the first explo)
 * @author Jehyanka
 */
public class ReceiveMessageBehaviour extends SimpleBehaviour{

	private static final long serialVersionUID = 9088209402507795289L;

	private boolean finished=false;
	
	public ReceiveMessageBehaviour(final Agent myagent) {
		super(myagent);
		
		setReceivers();
	}


	public void action() {
		
		BasicAgent agent= (BasicAgent)this.myAgent;
		ArrayList<Node> mapNodes=agent.getMyMap(); 
		
		if(agent.isEveryoneElseFinished()) {
			finished=true;
			System.out.println(" ++++++ "+this.myAgent.getLocalName()+" knows everyone else has finished the first explo ++++++ ");
		}
		//1) receive the map update
		final MessageTemplate msgTemplate = MessageTemplate.MatchPerformative(ACLMessage.INFORM) ;
		final ACLMessage msg = this.myAgent.receive(msgTemplate);
		
		if (msg != null) {
			if(agent.setAllOtherAgents().size()==0)
				setReceivers();
			//The message is either a map or a hello message
			
			if(!msg.getContent().contains("Hello")) {	
				//Cas du message de MAJ de la map
				if(!agent.isHasFinished()) {
					try {
						@SuppressWarnings("unchecked")
						ArrayList<Node> nodes = (ArrayList<Node>) msg.getContentObject();
						//System.out.println(this.myAgent.getLocalName()+"<----Map received from "+msg.getSender().getLocalName());
						for(Node node:nodes) {
							if(agent.getNodeById(node.getId())==null) {
								mapNodes.add(node);
								agent.addNodeToGraph(node);
							}
						}
						
					} catch (UnreadableException e) {
						System.out.println("A problem occured when receiving MapData");
						e.printStackTrace();

						System.out.println("Message RECEIVED : " +msg.getContent());
					}	
				}	
			}		
		}
		
		//1) receive the map request
		final MessageTemplate template = MessageTemplate.MatchPerformative(ACLMessage.REQUEST) ;
		final ACLMessage msg3 = this.myAgent.receive(template);
		
		if (msg3 != null) {
			agent.setOnPause(true);
			//System.out.println(this.myAgent.getLocalName()+"<----Map Request received from "+msg3.getSender().getLocalName());
			
			ArrayList<Node> nodesToSend=new ArrayList<Node>();
			ArrayList<AID> rec=new ArrayList<AID>();
			rec.add(msg3.getSender());
			try {
				@SuppressWarnings("unchecked")
				ArrayList<String> known_Nodes=(ArrayList<String>)msg3.getContentObject();
				for(Node n:mapNodes) {
					if(!known_Nodes.contains(n)) {
						nodesToSend.add(n);
					}
				}
				this.myAgent.addBehaviour(new SendMapDataBehaviour((mas.abstractAgent)this.myAgent, nodesToSend, rec));
			} catch (UnreadableException e) {
				e.printStackTrace();
			}
			
		}
		
	}
	

	public boolean done() {
		return finished;
	}
	
	
	private void setReceivers() {
		((BasicAgent)this.myAgent).setAllOtherAgents();
	}
	
	
	

}

