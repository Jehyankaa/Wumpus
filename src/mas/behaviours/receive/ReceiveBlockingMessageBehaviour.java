package mas.behaviours.receive;

import java.util.HashMap;

import jade.core.Agent;
import jade.core.behaviours.SimpleBehaviour;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.MessageTemplate;
import jade.lang.acl.UnreadableException;
import mas.agents.*;
/**
 *Receives blocking messages
 *@author Jehyanka
 */
public class ReceiveBlockingMessageBehaviour extends SimpleBehaviour{

	private static final long serialVersionUID = 9088209402507795289L;

	private boolean finished=false;

	/**
	 * It receives BlockingMessage with all the necessary details from the other agent
	 * @param myagent
	 */
	public ReceiveBlockingMessageBehaviour(final Agent myagent) {
		super(myagent);

	}


	public void action() {
		
		//To receive blocking messages
		final MessageTemplate msgTemplate1 = MessageTemplate.MatchPerformative(ACLMessage.INFORM_IF);		
		final ACLMessage msg1 = this.myAgent.receive(msgTemplate1);

		if (msg1 != null) {
			//System.out.println(this.myAgent.getLocalName()+"<----Blocking Message received from "+msg1.getSender().getLocalName());
			try {
				@SuppressWarnings("unchecked")
				HashMap<String,Object> msgContent = (HashMap<String,Object>)msg1.getContentObject();
				//System.out.println(this.myAgent.getLocalName()+" <--- has received : "+msgContent);
								
				//If he is the one who is blocking and he is blocked too(he doesn't move) --> we handle the Priority
				
				String myDestination="";
				int i = 0;
				do {
					if(i<((BasicAgent)this.myAgent).getPathToFollow().size())
						myDestination=((BasicAgent)this.myAgent).getPathToFollow().get(i);
					i++;
				}
				while(myDestination.equals(((BasicAgent)this.myAgent).getCurrentPosition()));
				
				boolean blocking=(msgContent.get("destination").equals(((BasicAgent)this.myAgent).getCurrentPosition()))
						&&(myDestination.equals((msgContent).get("position").toString()));
				
				if (blocking ) {
					System.out.println(msg1.getSender().getLocalName()+" VS "+this.myAgent.getLocalName());
					//System.out.println(this.myAgent.getLocalName()+" is blocking "+msg1.getSender().getLocalName());
					((BasicAgent)this.myAgent).addBlockedNeighbour(msgContent.get("position").toString());
					((BasicAgent)this.myAgent).handlePriority(msgContent);
				}
				

			} catch (UnreadableException e) {
				e.printStackTrace();
			}
						
		}
		
	}

	public boolean done() {
		return finished;
	}
	
	
	
	
	
	

}

