package mas.behaviours.send;

import java.io.IOException;
import java.util.ArrayList;

import jade.core.AID;
import jade.core.behaviours.SimpleBehaviour;
import jade.domain.DFService;
import jade.domain.FIPAException;
import jade.domain.FIPAAgentManagement.DFAgentDescription;
import jade.domain.FIPAAgentManagement.SearchConstraints;
import jade.domain.FIPAAgentManagement.ServiceDescription;
import jade.lang.acl.ACLMessage;
import mas.abstractAgent;
import mas.agents.BasicAgent;
import mas.data.Node;

/**
 * Send a part of the map around 
 * @author Jehyanka
 *
 */
public class SendMapDataBehaviour extends SimpleBehaviour {
	
	private static final long serialVersionUID = 1L;
	//private Node node;
	private ArrayList<Node> nodes;
	private boolean finished=false;
	private 	ArrayList<AID> receivers;
	/*public SendMapDataBehaviour(final mas.abstractAgent myagent, Node node) {
		super(myagent);
		this.node = node;
	}*/
	
	public SendMapDataBehaviour(final mas.abstractAgent myagent, ArrayList<Node> nodes, ArrayList<AID> receivers) {
		super(myagent);
		this.nodes = nodes;
		this.receivers=receivers;
	}

	@Override
	public void action() {
		
		
		//Create the message
		final ACLMessage msg = new ACLMessage(ACLMessage.INFORM);
		msg.setSender(this.myAgent.getAID());
		
		/*ArrayList<AID> allR=((BasicAgent)this.myAgent).getAllOtherAgents();
		for(AID a:allR) {
			msg.addReceiver(a);
		}*/
		for(AID a:receivers) {
			msg.addReceiver(a);
		}
		
		msg.setContent("MapData");
		try {
			msg.setContentObject(nodes);
		} catch (IOException e) {
			e.printStackTrace();
		}
		((abstractAgent)this.myAgent).sendMessage(msg);

		//System.out.println(this.myAgent.getLocalName()+" ---> has sent his Map");
		((BasicAgent)this.myAgent).setOnPause(false);
		finished=true;
		
	}

	@Override
	public boolean done() {
		return finished;
	}
	

}
