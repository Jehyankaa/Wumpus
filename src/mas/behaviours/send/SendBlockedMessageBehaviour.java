package mas.behaviours.send;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

import jade.core.AID;
import jade.core.behaviours.SimpleBehaviour;
import jade.domain.DFService;
import jade.domain.FIPAException;
import jade.domain.FIPAAgentManagement.DFAgentDescription;
import jade.domain.FIPAAgentManagement.SearchConstraints;
import jade.domain.FIPAAgentManagement.ServiceDescription;
import jade.lang.acl.ACLMessage;
import mas.abstractAgent;
import mas.agents.BasicAgent;
import mas.agents.BasicAgent.agentType;
import mas.agents.CollectorAgent;
import mas.data.Node;

/**
 * Send a message that indicates that we are blocked and all the details necessary to the handle of the priority
 * @author Jehyanka
 *
 */
public class SendBlockedMessageBehaviour extends SimpleBehaviour {

	
	private static final long serialVersionUID = 1L;
	private boolean finished=false;
	protected final String BLOCKED_MESSAGE="Hey stop blocking me !";
	private String destination="";
	private String origin="";
	
	
	public SendBlockedMessageBehaviour(final mas.abstractAgent myagent,String destination,String origin) {
		super(myagent);
		this.destination=destination;
		this.origin=origin;
		
	}
	
	@Override
	public void action() {
		BasicAgent ba=(BasicAgent)this.myAgent;
		if (!ba.getCurrentPosition().equals(origin) || !ba.isBlocked()) {
			finished=true;
			done();
		}
		else {
			//1°Create the message
			final ACLMessage msg = new ACLMessage(ACLMessage.INFORM_IF);
			msg.setSender(this.myAgent.getAID());
		
			
			ArrayList<AID> allR=((BasicAgent)this.myAgent).setAllOtherAgents();
			for(AID a:allR) {
				msg.addReceiver(a);
			}
			
			/**Setting details of myAgent to send (for blocking priority analysis)**/
			HashMap<String,Object> myAgentDetails = new HashMap<String,Object>();
			
			String type="explorer",position="";
			
			position=ba.getCurrentPosition();
			//Distance from goal
			int pathSize=ba.getPathToFollow().size();
			//Si je suis un collector
			if(ba.getType().equals(agentType.COLLECTOR)){
				//TODO Ajout des détails : quelqu'un d'autre me bloque ? 
				type="collector";
				CollectorAgent myCollectorAgent=((mas.agents.CollectorAgent)this.myAgent);
				int freeSpace=myCollectorAgent.getBackPackFreeSpace();
				myAgentDetails.put("backPackFreeSpace",freeSpace);
				
			}
			
			if(ba.getType().equals(agentType.SILO))
				type="silo";
			
			myAgentDetails.put("pathSize", pathSize);
			//Sinon je suis un explorateur (pour l'instant pas de silo TODO)
			myAgentDetails.put("destination",destination);
			
			myAgentDetails.put("type", type);
			
			Node n=ba.getNodeById(position);
			//DEAD END - cas où l'un des agents est à un noeud puit (pour l'instant on ne gère pas les autres agents bloquants)
			if(n.getNeighbours().size()==1)
				myAgentDetails.put("deadEnd", true);
			else
				myAgentDetails.put("deadEnd",false);
			
			if(ba.allNeighboursBlocked()) 
				myAgentDetails.put("neighbourhoodBlocked", true);
			else
				myAgentDetails.put("neighbourhoodBlocked", false);
			
			
			myAgentDetails.put("position", position);
			
			
			
			
			
			//Setting message and sending
			msg.setContent(BLOCKED_MESSAGE);
			try {
				msg.setContentObject(myAgentDetails);
			} catch (IOException e) {
				e.printStackTrace();
			}
			
			((abstractAgent) this.myAgent).sendMessage(msg);
	
			//System.out.println(this.myAgent.getLocalName()+" ---> has sent his details to surrounding agents (bcause he is blocked) ");
		}
		finished=true;
		

	}

	@Override
	public boolean done() {
		return finished;
	}

}
