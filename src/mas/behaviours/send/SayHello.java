package mas.behaviours.send;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

import jade.core.AID;
import jade.core.Agent;
import jade.core.behaviours.SimpleBehaviour;
import jade.core.behaviours.TickerBehaviour;
import jade.domain.DFService;
import jade.domain.FIPAException;
import jade.domain.FIPAAgentManagement.DFAgentDescription;
import jade.domain.FIPAAgentManagement.SearchConstraints;
import jade.domain.FIPAAgentManagement.ServiceDescription;
import jade.lang.acl.ACLMessage;
import mas.agents.BasicAgent;

/**
 * Sends CollectorsDetails
 * Two aims : propagate the collectorDetails list (so that the SILO will know about it)
 * 	& let everyone know that this agent has a full map and finished the first exploraiton.
 * @author Jehyanka
 *
 */
public class SayHello extends SimpleBehaviour{

	private static final long serialVersionUID = -2058134622078521998L;
	private boolean finished=false;

	public SayHello (final Agent myagent) {
		super(myagent);
	}

	@Override
	public void action() {
		String myPosition=((mas.abstractAgent)this.myAgent).getCurrentPosition();

		ACLMessage msg=new ACLMessage(ACLMessage.PROPAGATE);
		
		msg.setSender(this.myAgent.getAID());

		if (myPosition!=""){
			//System.out.println("Agent "+this.myAgent.getLocalName()+ " is trying to reach its friends");
			
			ArrayList<AID> allR=((BasicAgent)this.myAgent).setAllOtherAgents();
			if(((BasicAgent)this.myAgent).isCollector()) {
				//If i'm a collector myself I add/update my details.
				((BasicAgent)this.myAgent).addMyDetails();				
			}
			//System.out.println(this.myAgent.getLocalName()+" sends his details "+((BasicAgent)this.myAgent).getBackPackFreeSpace());
			try {
				msg.setContentObject(((BasicAgent)this.myAgent).getCollectorsDetails());
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			for(AID a:allR) {
				msg.addReceiver(a);
			}

			((mas.abstractAgent)this.myAgent).sendMessage(msg);
			
			finished=true;

		}

	}

	@Override
	public boolean done() {
		return finished;
	}

}