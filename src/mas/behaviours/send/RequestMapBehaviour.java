package mas.behaviours.send;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

import jade.core.AID;
import jade.core.behaviours.SimpleBehaviour;
import jade.lang.acl.ACLMessage;
import mas.abstractAgent;
import mas.agents.BasicAgent;
import mas.agents.CollectorAgent;
import mas.data.Node;

/**
 * Sends map request with the agent's known nodes
 * Will be sent while the map isn't completely set up
 * @author Jehyanka
 *
 */
public class RequestMapBehaviour extends SimpleBehaviour{
	
	private static final long serialVersionUID = 1L;

	private boolean finished=false;
	protected final static String REQUEST_MAP="Please send me the full map";
	public RequestMapBehaviour(final mas.abstractAgent myagent) {
		super(myagent);
		
	}
	@Override
	public void action() {
		
		
		BasicAgent ba=(BasicAgent)this.myAgent;
		//1°Create the message
		final ACLMessage msg = new ACLMessage(ACLMessage.REQUEST);
		msg.setSender(this.myAgent.getAID());
		
		for(AID a:ba.setAllOtherAgents()) {
			msg.addReceiver(a);
		}
		//Setting message and sending
		msg.setContent(REQUEST_MAP);
		
		ArrayList<String> known_Nodes=new ArrayList<String>();
		
		ArrayList<Node> nodes=((BasicAgent)this.myAgent).getMyMap();
		
		for(Node n: nodes) {
			known_Nodes.add(n.getId());
		}
		
		try {
			msg.setContentObject(known_Nodes);
			//msg.setContentObject(nodes);
		} catch (IOException e) {
			e.printStackTrace();
		}
		((abstractAgent) this.myAgent).sendMessage(msg);

		//System.out.println(this.myAgent.getLocalName()+" ---> has sent map Request ");
		
		
		finished=true;
		

	}
	
	
	
	
	@Override
	public boolean done() {
		return finished;
	}

}
