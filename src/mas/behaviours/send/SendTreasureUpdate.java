package mas.behaviours.send;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

import jade.core.AID;
import jade.core.behaviours.SimpleBehaviour;
import jade.lang.acl.ACLMessage;
import mas.abstractAgent;
import mas.agents.BasicAgent;
import mas.data.Node;
import mas.data.NodeDetails;


/**
 * Sends all his nodes with their amount of treasure/diamonds
 * This information will be sorted at the reception (according to when this info date back to)
 * @author Jehyanka
 *
 */
public class SendTreasureUpdate extends SimpleBehaviour {
	
	private static final long serialVersionUID = 1L;
	
	private boolean finished=false;
	private ArrayList<NodeDetails> listNodes=new ArrayList<NodeDetails>();

	
	public SendTreasureUpdate(final mas.abstractAgent myagent) {
		super(myagent);
		ArrayList<NodeDetails> nodes = ((BasicAgent)myagent).getNodeDetails();
		for(NodeDetails n : nodes) {
			listNodes.add(n);		
		}
	}

	@Override
	public void action() {
		
		//System.out.println(this.myAgent.getLocalName()+"----> Sends New Nodes ");
		//Create the message
		final ACLMessage msg = new ACLMessage(ACLMessage.INFORM_REF);
		msg.setSender(this.myAgent.getAID());
		
		ArrayList<AID> allR=((BasicAgent)this.myAgent).getAllOtherAgents();
		for(AID a:allR) {
			msg.addReceiver(a);
		}
		try {
			msg.setContentObject(listNodes);
		} catch (IOException e) {
			e.printStackTrace();
		}
		((abstractAgent)this.myAgent).sendMessage(msg);
		finished=true;
		
	}

	@Override
	public boolean done() {
		return finished;
	}
	

}
