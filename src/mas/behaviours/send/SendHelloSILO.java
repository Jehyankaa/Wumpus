package mas.behaviours.send;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

import jade.core.AID;
import jade.core.Agent;
import jade.core.behaviours.SimpleBehaviour;
import jade.core.behaviours.TickerBehaviour;
import jade.domain.DFService;
import jade.domain.FIPAException;
import jade.domain.FIPAAgentManagement.DFAgentDescription;
import jade.domain.FIPAAgentManagement.SearchConstraints;
import jade.domain.FIPAAgentManagement.ServiceDescription;
import jade.lang.acl.ACLMessage;
import mas.agents.BasicAgent;

/**
 * ONLY for the SILO
 * Will send a special hello to let everyone know where he is and that he is the SILO
 * @author Jehyanka
 *
 */
public class SendHelloSILO extends SimpleBehaviour{

	private static final long serialVersionUID = -2058134622078521998L;
	private boolean finished=false;

	/**
	 * SILO is telling where he is
	 *  
	 */
	public SendHelloSILO (final Agent myagent) {
		super(myagent);
	}

	@Override
	public void action() {
		String myPosition=((mas.abstractAgent)this.myAgent).getCurrentPosition();

		ACLMessage msg=new ACLMessage(ACLMessage.PROPAGATE);
		
		msg.setSender(this.myAgent.getAID());

		if (myPosition!=""){
			//System.out.println("Agent "+this.myAgent.getLocalName()+ " is trying to reach its friends");
			
			ArrayList<AID> allR=((BasicAgent)this.myAgent).setAllOtherAgents();
			msg.setContent("Hello I'm at "+myPosition);
			for(AID a:allR) {
				msg.addReceiver(a);
			}

			((mas.abstractAgent)this.myAgent).sendMessage(msg);
			
			finished=true;

		}

	}

	@Override
	public boolean done() {
		return finished;
	}

}