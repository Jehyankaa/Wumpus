package mas.behaviours;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import env.Attribute;
import env.Couple;
import jade.core.behaviours.TickerBehaviour;
import mas.agents.BasicAgent;
import mas.behaviours.receive.ReceiveBlockingMessageBehaviour;
import mas.behaviours.receive.ReceiveHelloBehaviour;
import mas.behaviours.receive.ReceiveMessageBehaviour;
import mas.behaviours.receive.ReceiveNodesBehaviour;
import mas.behaviours.send.RequestMapBehaviour;
import mas.data.Node;

/**
 * FirstExploration - Recherche de noeuds et constitution de la map
 * @author Jehyanka
 *
 */
public class FirstExplorationBehaviour extends TickerBehaviour {

	
	private static final long serialVersionUID = 9088209402507795289L;
	//If every node around the agent is closed he goes (randomly) to search a new node
	//While this boolean is false he will follow a Depth First Search
	private boolean searchingForNewNode=false;
	
	private boolean launched=false;
	public FirstExplorationBehaviour (final mas.abstractAgent myagent) {
		super(myagent, 100);
	}

	@Override
	public void onTick() { 
		
		//Cast to make agent use easier.
		BasicAgent basicAgent=(BasicAgent)this.myAgent;
		
		//For fsm
		//adding receive behaviours once
		if(!launched) {
			//System.out.println("Je lance la premiere explo "+basicAgent.getLocalName());
			basicAgent.addBehaviour(new ReceiveMessageBehaviour(basicAgent));
			basicAgent.addBehaviour(new ReceiveHelloBehaviour(basicAgent));
			basicAgent.addBehaviour(new ReceiveBlockingMessageBehaviour(basicAgent));
			basicAgent.addBehaviour(new ReceiveNodesBehaviour(basicAgent));
			launched=true;
		}

		String myPosition=((mas.abstractAgent)this.myAgent).getCurrentPosition();
		ArrayList<Node> mapNodes=basicAgent.getMyMap();
		//System.out.println(basicAgent.getLocalName()+"'s map size is : " +mapNodes.size());
		boolean isFinished=(basicAgent.setHasFinished());
		
		/**First Exploration is done (i.e internal map is complete)**/
		if (isFinished) {
			stop();
			System.out.println("*********Premier parcours d'explo fini pour"+ this.myAgent.getLocalName()+"!\n********");	
			basicAgent.addBehaviour(new SetStrategyBehaviour(basicAgent));
		}
		
		
		if (myPosition!=""){
			List<Couple<String,List<Attribute>>> lobs=((mas.abstractAgent)this.myAgent).observe();//myPosition			
			ArrayList<String> voisins= new ArrayList<String>();
			
			for (Couple<String,List<Attribute>> c:lobs) {
				voisins.add(c.getLeft());
			}
			//Remove current node from neighbours
			voisins.remove(0);
			//Creating a new node
			Node n= new Node(lobs.get(0).getLeft(),voisins);
		
			//list of attribute associated to the currentPosition
			List<Attribute> lattribute= lobs.get(0).getRight();			
			if(!lattribute.isEmpty()) {
				for(Attribute at:lattribute){
					switch (at) {
					case TREASURE:
						n.setTreasure((Integer)at.getValue());
						break;
					case DIAMONDS:
						n.setDiamonds((Integer)at.getValue());
						break;
					default:
						break;
					}
				}						
			}
			
			//If node not already known, it's added to the map (and to the dynamic graph)
			if (basicAgent.isOpen(n.getId())) {
				mapNodes.add(n);
				basicAgent.addNodeToGraph(n);
			}
			
			
			/**DFS**/
			String currentPos=myPosition;
			List<String> path=basicAgent.getPath();
			
			if (!path.contains(currentPos)) {
				path.add(currentPos);
			}
			
			//Forward
			String destination="";
			//get one opened neighbour node
			for(int i=1;i<lobs.size();i++) {
				if(basicAgent.getNodeById(lobs.get(i).getLeft())==null) {
					destination=lobs.get(i).getLeft();
					break;
				}
			};
			
			int index=path.size();
			String previousNode= basicAgent.getPreviousNode();
			
			//Backward
			//If no opened node around, we go backward
			if (destination.equals("") ) {
				
				if(path.contains(currentPos))
					 index=path.lastIndexOf(currentPos);
				
				//If we are not at the beginning of our path and there is ('a priori') opened nodes backward,
				//We go back.
				if(index>0 && !searchingForNewNode) {
					destination = path.get((index-1));
					path.remove(index);
				}
				
				else {
					//Random neighbour chosen
					Random rand=new Random();
					int i;
					int size=voisins.size();
					do {
						i=rand.nextInt(size);
						destination=voisins.get(i);
						
					}while(size>1 && previousNode.equals(destination));
					searchingForNewNode=true;
				}
				
			}
			else
				searchingForNewNode=true;
			
			//Update agent's data and sending around.
			basicAgent.setPath(path);
			basicAgent.setPreviousNode(currentPos);
			basicAgent.addBehaviour(new RequestMapBehaviour(basicAgent));
			//Little pause to allow you to follow what is going on
			/*try {
				System.out.println("Press Enter in the console to allow the agent "+this.myAgent.getLocalName() +" to execute its next move");
				System.in.read();
			} catch (IOException e) {
				e.printStackTrace();
			}*/
			
			//Interblocking is not handled during the first explo
			if(!basicAgent.isOnPause())
				basicAgent.moveTo(destination);
			
		}
		
		
	}
	
}
