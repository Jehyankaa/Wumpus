package mas.behaviours;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import env.Attribute;
import env.Couple;
import jade.core.behaviours.TickerBehaviour;
import mas.agents.BasicAgent;
import mas.behaviours.send.SendHelloSILO;
import mas.behaviours.send.SendTreasureUpdate;
import mas.data.NodeDetails;

/**
 * follow a path till destination reached
 * Once the destination is reached, we can pick up (if collector), or go on with another strategy
 * @author Jehyanka
 *
 */

public class GoToBehaviour extends TickerBehaviour{
	/**
	 * When an agent goes to a destination (therefore follows a path)
	 *  
	 */
	private static final long serialVersionUID = 9088209402507795289L;
	private String destination="";
	private ArrayList<String> path=new ArrayList<String>();
	private BasicAgent ba;
	
	public GoToBehaviour (final mas.abstractAgent myagent) {
		super(myagent, 100);		
		ba=(BasicAgent)myagent;
	}
	
	public GoToBehaviour (final mas.abstractAgent myagent, String destination) {
		super(myagent, 1000);
		ba=(BasicAgent)myagent;
		this.destination=destination;
		System.out.println(myagent.getLocalName()+" wants to go to : "+destination);
		recalculatePath(destination);
		
		
	}

	@Override
	public void onTick() {
		
		
		BasicAgent ba=(mas.agents.BasicAgent)this.myAgent;
		String myPosition=ba.getCurrentPosition();
		ba.setBlocked(false);
		int treasure=0,diamonds=0;
		boolean reached=false;
		
		if(destination.length()==0)
			destination=ba.getDestination();
		
		if (myPosition!=""){
			if(ba.getType().equals(BasicAgent.agentType.SILO)) {
				String dest=ba.getBestCollectorPosition();
				if(dest.length()>0) {
					destination=dest;
					recalculatePath(destination);
				}
			}
			List<Couple<String,List<Attribute>>> lobs=((mas.abstractAgent)this.myAgent).observe();//myPosition
			List<Attribute> lattribute= lobs.get(0).getRight();			
			for(Attribute a:lattribute){
				switch (a) {
				case TREASURE:
					treasure=(int)a.getValue();
					break;
				case DIAMONDS:
					diamonds=(int)a.getValue();
					
				default:
					break;
				}
			}
			NodeDetails nd=new NodeDetails(myPosition);
			nd.setDate();
			nd.setDiamonds(diamonds);
			nd.setTreasure(treasure);

			ba.addNodeDetails(nd);
			
			
			
			if(reachedDestination()) {
				
				reached=true;
				path.clear();
				System.out.println(ba.getLocalName()+ " est ARRIVÉ A DESTINATION !!");
				stop();
				//ba.setBlocked(true);
				if(ba.isCollector()) 
					pickUp();
				ba.addBehaviour(new SetStrategyBehaviour(ba));
								
			}
			
			ba.addBehaviour(new SendTreasureUpdate(ba));

			if(ba.getType().equals(BasicAgent.agentType.SILO)) {
				ba.addBehaviour(new SendHelloSILO(ba));
			}
			else {
				ba.addBehaviour(new mas.behaviours.send.SayHello(ba));
			}
			
			if(!reached){
				//ba.printNodeDetails();
				if(!correctPath()) {
					recalculatePath(destination);
				}
				if(myPosition.equals(path.get(0)))	
					path.remove(0);
				
				ba.setPreviousNode(myPosition);
				ba.tryToGoTo(path.get(0));
			}
			
			//Little pause to allow you to follow what is going on
			/*try {
				System.out.println("Press Enter in the console to allow the agent "+this.myAgent.getLocalName() +" to execute its next move");
				System.in.read();
			} catch (IOException e) {
				e.printStackTrace();
			}*/
			
			
			
			
			
			
		}
			
		
	}
	private void pickUp() {
		
		ba.pick();
		List<Couple<String,List<Attribute>>> lobs2=((mas.abstractAgent)this.myAgent).observe();//myPosition
		NodeDetails temp;
		for (Couple<String,List<Attribute>> c:lobs2) {
			int treasure=0,diamonds=0;
			List<Attribute> lattribute2= c.getRight();	
			for(Attribute a:lattribute2){
				switch (a) {
				case TREASURE:
					treasure=(int)a.getValue();
					break;
				case DIAMONDS:
					diamonds=(int)a.getValue();
				default:
					break;
				}
			}	
			temp=new NodeDetails(c.getLeft());
			temp.setDate();
			temp.setDiamonds(diamonds);
			temp.setTreasure(treasure);
			ba.addNodeDetails(temp);

		}
	}
	
	public boolean reachedDestination() {
		return (destination.equals(ba.getCurrentPosition()));
	}
	//If the path was not initialized or if the path don't go to the destination (wrong path in memory)
	public boolean correctPath() {
	
		if(path.size()==0)
			return false;
		return (path.get(path.size()-1).equals(destination));
	}
	
	public void recalculatePath(String destination) {
		if (ba.getCurrentPosition().equals(destination))
			return;

		this.destination=destination;
		ba.setPathToFollow(destination);
		path=ba.getPathToFollow();
		Collections.reverse(path);
		//System.out.println(path);
	}
	
	
	
	
	
	
}