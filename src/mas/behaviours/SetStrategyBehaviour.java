package mas.behaviours;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import env.Attribute;

import env.Couple;
import jade.core.AID;
import jade.core.behaviours.SimpleBehaviour;
import jade.core.behaviours.TickerBehaviour;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.MessageTemplate;
import mas.agents.BasicAgent;
import mas.agents.BasicAgent.agentType;
import mas.behaviours.receive.ReceiveHelloBehaviour;

/**
 * Behaviour qui determine la stratégie avant de bouger.
 * On choisit une destination en fonction du type d'agent et de la situation avant de s'y rendre
 * @author Jehyanka
 *
 */

public class SetStrategyBehaviour extends SimpleBehaviour{
	

	/**
	 * 
	 *  
	 */
	private static final long serialVersionUID = 9088209402507795289L;
	private boolean finished=false;
	private String destination="";
	
	
	public SetStrategyBehaviour (final mas.abstractAgent myagent) {	
		super(myagent);
		
	}

	@Override
	public void action() {		
		BasicAgent agent=((mas.agents.BasicAgent)this.myAgent);
		agentType type=agent.getType();
		//System.out.println("Determination de la stratégie pour "+agent.getLocalName());

		//Example to retrieve the current position
		String myPosition=((mas.abstractAgent)this.myAgent).getCurrentPosition();
		Random r= new Random();
		if (myPosition!=""){
			switch(type) {
			case COLLECTOR:
				destination=agent.getBestNode();	
				break;
				
			//EXPLORER gets a random node among the actual neighbours
			case EXPLORER:
				List<Couple<String,List<Attribute>>> lobs=((mas.abstractAgent)this.myAgent).observe();			
				int moveId=r.nextInt(lobs.size());				
				destination= lobs.get(moveId).getLeft();
				break;
			//SILO gets the one who needs  to empty his back i.e the one with the minimum difference |MaxbackPackCapacity-freeCapacity| 
			case SILO:
				destination=agent.getBestCollectorPosition();
				System.out.println("Je vais chercher le collecteur en "+destination);
				break;
			default:
				break;
			}
			
			//TODO change random -> choose an interesting node
			if(destination.length()==0) {
				System.out.println("pas de noeud interessant so random");
				int randIndex=r.nextInt(agent.getMyMap().size());				
				destination= agent.getMyMap().get(randIndex).getId();
			}
			
			//agent.setPathToFollow(destination);
			agent.setDestination(destination);

			//System.out.println(agent.getLocalName()+" is going to the best node which is "+destination);
			agent.addBehaviour(new GoToBehaviour(agent,destination));				
			finished=true;
						
						
		}
		
		
		
	}

	@Override
	public boolean done() {
		return finished;
	}
	
	
		
	
	
	
}