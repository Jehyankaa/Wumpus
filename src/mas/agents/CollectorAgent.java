package mas.agents;

import mas.data.Node;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import env.Attribute;
import env.Couple;
import jade.core.behaviours.SimpleBehaviour;
import jade.core.behaviours.TickerBehaviour;

public class CollectorAgent extends BasicAgent{

	/**
	 * Agent that can collect one type of treasure
	 */
	private static final long serialVersionUID = -1784844593772918359L;



	/**
	 * This method is automatically called when "agent".start() is executed.
	 * Consider that Agent is launched for the first time. 
	 * 			1) set the agent attributes 
	 *	 		2) add the behaviours
	 *          
	 */
	protected void setup(){

		super.setup();

		registerOnDF("collector");
		//Register on the DF
		
		//addBehaviour(new GetPositionBehaviour(this));
		//super.setBlocked(true);
	}

	/**
	 * This method is automatically called after doDelete()
	 */
	protected void takeDown(){

	}


/**************************************
 * 
 * 
 * 				BEHAVIOUR
 * 
 * 
 **************************************/
class GetPositionBehaviour extends SimpleBehaviour{
	/**
	 * When an agent choose to move
	 *  
	 */
	private boolean finished=false;
	private static final long serialVersionUID = 9088209402507795289L;
	public GetPositionBehaviour (final mas.abstractAgent myagent) {
		super(myagent);
		
	}

	@Override
	public void action() {
		BasicAgent ba=(mas.agents.BasicAgent)this.myAgent;
		String myPosition=ba.getCurrentPosition();
		if(myPosition.equals(ba.getPreviousNode()))
				finished=true;

		ArrayList<Node> mapNodes=ba.getMyMap();
		
		if (myPosition!=""){
			//List of observable from the agent's current position
			List<Couple<String,List<Attribute>>> lobs=ba.observe();//myPosition
			ArrayList<String> voisins= new ArrayList<String>();
			for (Couple<String,List<Attribute>> c:lobs) {
				voisins.add(c.getLeft());
			}
			voisins.remove(0);
			
			Node n= new Node(lobs.get(0).getLeft(),voisins);
			if (((mas.agents.BasicAgent)this.myAgent).getNodeById(n.getId())==null) {
				mapNodes.add(n);
			}
		}
		
		ba.setPreviousNode(myPosition);
	}

	@Override
	public boolean done() {
		return finished;
	}
}

class RandomWalkExchangeBehaviour extends TickerBehaviour{
	/**
	 * When an agent choose to move
	 *  
	 */
	private static final long serialVersionUID = 9088209402507795289L;

	public RandomWalkExchangeBehaviour (final mas.abstractAgent myagent) {
		super(myagent, 600);	
	}

	@Override
	public void onTick() {
		//Example to retrieve the current position
		String myPosition=((mas.abstractAgent)this.myAgent).getCurrentPosition();

		ArrayList<Node> mapNodes=((mas.agents.BasicAgent)this.myAgent).getMyMap();
		
		if (myPosition!=""){
			//List of observable from the agent's current position
			List<Couple<String,List<Attribute>>> lobs=((mas.abstractAgent)this.myAgent).observe();//myPosition
			System.out.println(this.myAgent.getLocalName()+" -- list of observables: "+lobs);

			//list of attribute associated to the currentPosition
			List<Attribute> lattribute= lobs.get(0).getRight();

			ArrayList<String> voisins= new ArrayList<String>();
			for (Couple<String,List<Attribute>> c:lobs) {
				voisins.add(c.getLeft());
			}
			voisins.remove(0);
			
			Node n= new Node(lobs.get(0).getLeft(),voisins);
			if (((mas.agents.BasicAgent)this.myAgent).getNodeById(n.getId())==null) {
				mapNodes.add(n);
			}
			//example related to the use of the backpack for the treasure hunt
			Boolean b=false;
			for(Attribute a:lattribute){
				switch (a) {
				case TREASURE : case DIAMONDS :
					System.out.println("My treasure type is :"+((mas.abstractAgent)this.myAgent).getMyTreasureType());
					System.out.println("My current backpack capacity is:"+ ((mas.abstractAgent)this.myAgent).getBackPackFreeSpace());
					System.out.println("Value of the treasure on the current position: "+a.getName() +": "+ a.getValue());
					System.out.println("The agent grabbed :"+((mas.abstractAgent)this.myAgent).pick());
					System.out.println("the remaining backpack capacity is: "+ ((mas.abstractAgent)this.myAgent).getBackPackFreeSpace());
					//System.out.println("The value of treasure on the current position: (unchanged before a new call to observe()): "+a.getValue());
					b=true;
					break;

				default:
					break;
				}
			}

			//If the agent picked (part of) the treasure
			if (b){
				List<Couple<String,List<Attribute>>> lobs2=((mas.abstractAgent)this.myAgent).observe();//myPosition
				System.out.println("lobs after picking "+lobs2);
			}
			System.out.println(this.myAgent.getLocalName()+" - My current backpack capacity is:"+ ((mas.abstractAgent)this.myAgent).getBackPackFreeSpace());
			System.out.println(this.myAgent.getLocalName()+" - The agent tries to transfer is load into the Silo (if reachable); succes ? : "+((mas.abstractAgent)this.myAgent).emptyMyBackPack("Agent5"));
			System.out.println("My current backpack capacity is:"+ ((mas.abstractAgent)this.myAgent).getBackPackFreeSpace());

			//Random move from the current position
			Random r= new Random();
			int moveId=1+r.nextInt(lobs.size()-1);

			//The move action (if any) should be the last action of your behaviour
			((mas.abstractAgent)this.myAgent).moveTo(lobs.get(moveId).getLeft());
		}

	}

}
}
