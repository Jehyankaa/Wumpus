package mas.agents;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Random;
import env.EntityType;
import env.Environment;
import graphDisplay.GraphDisplayer;
import jade.core.AID;
import jade.core.behaviours.FSMBehaviour;
import jade.domain.DFService;
import jade.domain.FIPAException;
import jade.domain.FIPAAgentManagement.DFAgentDescription;
import jade.domain.FIPAAgentManagement.SearchConstraints;
import jade.domain.FIPAAgentManagement.ServiceDescription;
import mas.abstractAgent;
import mas.behaviours.FirstExplorationBehaviour;
import mas.behaviours.send.SendBlockedMessageBehaviour;
import mas.data.CollectorDetails;
import mas.data.DjikstraObject;
import mas.data.Node;
import mas.data.NodeDetails;

/**
 * Agent basique (n'importe quel joueur sauf le wumpus)
 * Accès à sa map, garde chemin parcouru en mémoire, booleens indiquant son état et autres fonctions utiles
 * @author Jehyanka
 *
 */

public class BasicAgent extends abstractAgent implements Serializable{
	
	private static final long serialVersionUID = -1784844593772918359L;
	//Type de l'agent : EXPLO , COLLECTOR , SILO (ou autre)
	private agentType type=agentType.NONE;
	
	//Chemin suivi - utile pour retourner en arrière
	private List<String> path=new ArrayList<String> ();
	//Noeud précedent
	private String previousNode ="";
	private ArrayList<Node> myMap;//Ensemble des noeuds connus avec leur voisins
	//Il n'y a donc pas besoin de state Open/closed. Car les noeuds entrées dans la map sont fermés et le reste ouvert 
	//On peut accéder aux noeuds ouverts par les voisins 
	
	private GraphDisplayer myGraph;
	private boolean displayMyGraph=false;
	
	//Indique si l'agent est bloqué/immobilisé au noeud courant (soit par contrainte soit parce qu'il n'a rien d'autre à faire)
	private boolean blocked=false;
	//List des agents voisins qu'il bloque (il ne peut donc pas y aller)
	private ArrayList<String> blockedNeighbours=new ArrayList<String>();
	
	private boolean hasFinished=false;//If the first explo is done i.e the agent has the full map registered (no unknown nodes)
	private boolean everyoneElseFinished=false; //If everyone else has sent hello, then we know they are finished
	private boolean onPause=false;
	
	private String destination="";
	//Pour faire le rapport entre maxCapacite et capaciteLibre -> Sera plus pertinent que seulement la capacite libre
	private int maxCapacity=0;
	
	//Détails des collecteurs (où sont-ils et l'état de leur sac à dos) -> Information qui sera propagé dans le but d'atteindre le SILO
	private ArrayList<CollectorDetails> collectorsDetails=new ArrayList<CollectorDetails>();
	
	//Liste de tout les autres agents (explo/collector/silo) -> Utile pour l'envoi de messages
	private ArrayList<AID> allOtherAgents=new ArrayList<AID>();
	//Liste des agents ayant terminé l'exploration primaire.
	private ArrayList<AID> finishedAgents=new ArrayList<AID>();
	
	//When the agent has to go to a known node - he will follow this path
	private ArrayList<String> pathToFollow=new ArrayList<String>(); 	
	//One object for Djikstra Algo
	private DjikstraObject dObj=null;
	//List des détails des noeuds en terme de trésors (moins lourds que liste de Node)
	private ArrayList<NodeDetails> nodeDetails=new ArrayList<NodeDetails>();
	
	
	public boolean isCollector() {
		return this.type.equals(agentType.COLLECTOR);
	}
	public ArrayList<CollectorDetails> getCollectorsDetails() {
		return collectorsDetails;
	}

	public void setCollectorsDetails(ArrayList<CollectorDetails> collectorsCapacity) {
		this.collectorsDetails = collectorsCapacity;
	}
	
	
	public String getDestination() {
		return destination;
	}

	public void setDestination(String destination) {
		this.destination = destination;
		setPathToFollow(destination);
	}

	public boolean isOnPause() {
		return onPause;
	}

	public void setOnPause(boolean onPause) {
		this.onPause = onPause;
	}

	
	
	public ArrayList<AID> getFinishedAgents() {
		return finishedAgents;
	}

	public void setFinishedAgents(ArrayList<AID> finishedAgents) {
		this.finishedAgents = finishedAgents;
	}

	public ArrayList<String> getPathToFollow() {
		return pathToFollow;
	}

	public void setPathToFollow(String destination) {
		
		dObj=new DjikstraObject(this.getCurrentPosition(),myMap);
		this.pathToFollow = dObj.getPath(destination);
	}


	protected void registerOnDF(String type) {
		DFAgentDescription dfd = new DFAgentDescription();
        dfd.setName(getAID());
        ServiceDescription sd  = new ServiceDescription();
        sd.setType( type ); // name for the services we give 
        sd.setName(getLocalName() );
        dfd.addServices(sd);
        try {
          DFService.register(this, dfd );
        } catch (FIPAException fe) { fe.printStackTrace(); }
	}
	
	public boolean isHasFinished() {
		return hasFinished;
	}

	public boolean setHasFinished() {
		
		if(myMap.size()==0) {
			this.hasFinished = false;
			return false;
		}
			
		//If all nodes are there, everything has been explored ! (i.e for every node, all of his neighbours are in the map)
		for (Node node : myMap) {
			for (String c:node.getNeighbours()) {
				if (getNodeById(c)==null) {
					this.hasFinished = false;
					return false;
				}
			}
			
		}
		/*if(!this.hasFinished) {
			addBehaviour(new ParcoursGrilleBehaviour(this));
			addBehaviour(new ReceiveHelloBehaviour(this));
		}
		*/
		
		this.hasFinished = true;
		return true;
		
	}
	
	public ArrayList<Node> getMyMap() {
		return myMap;
	}

	public void setMyMap(ArrayList<Node> myMap) {
		this.myMap = myMap;
	}

	public List<String> getPath() {
		return path;
	}

	public String getPreviousNode() {
		return previousNode;
	}

	public void setPreviousNode(String previousNode) {
		this.previousNode = previousNode;
	}

	public void setPath(List<String> path) {
		this.path = path;
	}
	
	

	public boolean isBlocked() {
		return blocked;
	}

	public void setBlocked(boolean blocked) {
		this.blocked = blocked;
	}


	/**
	 * This method is automatically called when "agent".start() is executed.
	 * Consider that Agent is launched for the first time. 
	 * 			1) set the agent attributes 
	 *	 		2) add the behaviours
	 *          
	 */
	protected void setup(){

		super.setup();
		
		switch(getClass().getName()) {
		
		case "mas.agents.CollectorAgent":
			type=agentType.COLLECTOR;
			
			break;
		case "mas.agents.ExploAgent":
			type=agentType.EXPLORER;
			break;
		case "mas.agents.TankerAgent":
			type=agentType.SILO;
			
			break;
			
		}
		myMap=new ArrayList<Node>();
		if(displayMyGraph)
			myGraph=new GraphDisplayer();

		FSMBehaviour f=new FSMBehaviour(this) {
			
			private static final long serialVersionUID = 1L;

			public int onEnd() {
				System.out.println("FSM behaviour completed.");
				myAgent.doDelete();
				return super.onEnd();
			}
		};
		//f.registerFirstState(new FirstExplorationBehaviour(this), "FIRST");
		//f.registerState(new SetStrategyBehaviour(this),"STRATEGY");
		//f.registerState(new ReceiveHelloBehaviour(this),"RECEIVE_HELLO");
		//f.registerState(new GoToBehaviour(this), "GOTO");
		// Register the transitions
		f.registerDefaultTransition("FIRST", "STRATEGY");
		
		//f.registerTransition("STRATEGY", "STRATEGY", 0);
		//f.registerTransition("STRATEGY", "GOTO", 1);
		//f.registerTransition("GOTO", "STRATEGY",1);
		/*f.registerDefaultTransition("STRATEGY", "GOTO");
		f.registerDefaultTransition("GOTO", "STRATEGY");*/
		
		
		//get the parameters given into the object[]. In the current case, the environment where the agent will evolve
		final Object[] args = getArguments();
		if(args!=null && args[0]!=null && args[1]!=null){
			deployAgent((Environment) args[0],(EntityType)args[1]);

		}else{
			System.err.println("Malfunction during parameter's loading of agent"+ this.getClass().getName());
			System.exit(-1);
		}
		
		//addBehaviour(f);

		//Add the behaviours
		addBehaviour(new FirstExplorationBehaviour(this));
		
		System.out.println("the agent "+this.getLocalName()+ " is started");
		
		maxCapacity=this.getBackPackFreeSpace();
		
		
		
	}

	/**
	 * This method is automatically called after doDelete()
	 */
	protected void takeDown(){

	}
	 
	/**
	 * Ajoute les détails d'un noeud dans la liste (efface les anciennes données par rapport à ce noeud)
	 * @param nd
	 */
	public void addNodeDetails(NodeDetails nd) {
		NodeDetails temp=getNodeDetailsById(nd.getId());
		if(temp!=null)
			removeNodeDetails(temp);
		nodeDetails.add(nd);
		Node n=getNodeById(nd.getId());
		n.setDiamonds(nd.getDiamonds());
		n.setTreasure(n.getTreasure());
		setNode(n);
		
	}
	
	public void removeNodeDetails(NodeDetails nd) {
		
		nodeDetails.remove(nd);
		
	}
	
	
	public ArrayList<NodeDetails> getNodeDetails() {
		return nodeDetails;
	}

	public void setNodeDetails(ArrayList<NodeDetails> nodeDetails) {
		this.nodeDetails = nodeDetails;
	}

	public Node getNodeById(String id) {
		
		for(Node n:myMap) {
			if (n.getId().equals(id))
				return n;
		}
		return null;
	}
	
	public NodeDetails getNodeDetailsById(String id) {
		
		for(NodeDetails n:nodeDetails) {
			if (n.getId().equals(id))
				return n;
		}
		return null;
	}
	
	
	
	/**
	 * Recherche un voisin aléatoire du noeud
	 * @param node
	 * @return
	 */
	public String getRandomNeighbour(String node) {
		Random rand=new Random();
		String res="";
		Node n=getNodeById(node);
		ArrayList<String> neigh=n.getNeighbours();
		int index = rand.nextInt(neigh.size());
		res=neigh.get(index);
		return res;
		
	}
	
	/**
	 * On essaie d'atteindre la destination et si le déplacement a été un échec on diffuse le message de blocage.
	 * @param destination
	 */
	public void tryToGoTo(String destination) {
		if(!onPause) {
			String curr=getCurrentPosition();
			if(!getNodeById(curr).getNeighbours().contains(destination)) {
				if(!pathToFollow.get(0).equals(destination))
					setPathToFollow(destination);
				int lastIndex=pathToFollow.size()-1;
				destination=pathToFollow.get(lastIndex);
				pathToFollow.remove(lastIndex);
			}
				
			moveTo(destination);
			//Cas d'interblocage
			if(!getCurrentPosition().equals(destination)) {
				System.out.println("INTERBLOCAGE : J'essaie d'aller de "+getCurrentPosition()+" à " +destination);
				blocked=true;
				addBehaviour(new SendBlockedMessageBehaviour(this,destination,getCurrentPosition()));
			}
			//Déplacement réussi
			else {
				previousNode=curr;
				blockedNeighbours.clear();
				blocked=false;
			}
			
			
			//Dans tous les cas si on n'a pas d'endroit où aller on est "bloqué" à un noeud
			if(pathToFollow.size()==0)
				blocked=true;
			else {
				if(pathToFollow.get(0).equals(getCurrentPosition())) {
					blocked=true;
					pathToFollow.remove(0);
				}
			}
		}
	}
	
	/**
	 * Gère la priorité lors de l'interblocage. Si je n'ai pas la priorité, je me déplace vers un noeud non bloqué.
	 * @param details
	 */
	public void handlePriority(HashMap<String,Object> details) {
		boolean myPriority = priorityToMe(details);
		System.out.println("Does "+getLocalName()+" have the priority ? : "+myPriority);
		if(!myPriority) {
			String currentPosition=getCurrentPosition();
			String destination="";
			do {
				destination=getRandomNeighbour(currentPosition);
			} while (details.get("position").equals(destination) || blockedNeighbours.contains(destination));
				
			System.out.println("INTERBLOCAGE REPONSE :" + this.getLocalName()+" va bouger vers "+destination);
			tryToGoTo(destination);
		}
		else
			blocked=false;
	}
	
	
	/**
	 * Determine l'agent prioritaire 
	 * Selon critères
	 * @param ag2Details
	 * @return
	 */
	private boolean priorityToMe(HashMap<String,Object> ag2Details){

		String typeOfOtherAgent=(String) ag2Details.get("type");
		//Indique si les deux agents sont des collecteurs
		boolean bothCollector= (this.isCollector() 
				&&  (typeOfOtherAgent.equals(agentType.COLLECTOR)) ) ;

		if(bothCollector)	
			System.out.println("They are booth collectors");
		Node n=getNodeById(getCurrentPosition());
		
		//DEAD END - cas où l'un des agents est à un noeud puit  - i.e il n'a qu'un voisin (celui vers lequel il se dirige)
		if(n.getNeighbours().size()==1)
			return true;


		if((boolean)ag2Details.get("deadEnd"))
			return false;
		
		//Si l'agent n'a nulle part où aller (pas de but à suivre)
		if (pathToFollow.size()==0)
			return false;
		
		/*(Pour l'instant - On ne gère pas le voisinage complètement bloqué par d'autres agents)

		if((boolean)ag2Details.get("neighbourhoodBlocked"))
			return false;
		
		if(allNeighboursBlocked())
			return true;*/

		//Collecteurs ont la priorité sur les explorer et les SILO
		if (type.equals(agentType.COLLECTOR) && !bothCollector) 
			return true;				
					
		if (!bothCollector 
				&& (ag2Details.get("type").equals("collector"))) {
			return false;
			
		}
		

		//Distance du but restant
		int hisPathSize=(int)ag2Details.get("pathSize");
		if(getPathToFollow().size()>hisPathSize)
			return false;
		if(getPathToFollow().size()<hisPathSize)
			return true;
		
		//Capacité restante dans le sac
		if (bothCollector) {
			CollectorAgent collectorAgent=((mas.agents.CollectorAgent)this);
			int freeSpace1 = collectorAgent.getBackPackFreeSpace();
			int freeSpace2 = (int)ag2Details.get("backPackFreeSpace");
			return (freeSpace1>freeSpace2);
		}


		//TODO Topologie du graphe (cas où de blocage par d'autres agents)
		//TODO Fonction avec coeff pour l'importance des critères

		return false;
	}
	
	
	public void addBlockedNeighbour(String position) {
		if(!blockedNeighbours.contains(position))
			blockedNeighbours.add(position);
	}
	
	public boolean allNeighboursBlocked() {
		Node n=getNodeById(getCurrentPosition());
		for(String v:n.getNeighbours()) {
			if(!blockedNeighbours.contains(v))
				return false;
		}
		return true;
	}
	

	public ArrayList<AID> getAllOtherAgents() {
		return this.allOtherAgents;
	}
	
	/**
	 * Finds silo, all explorer and collector in the DF
	 * @return
	 */
	public ArrayList<AID> setAllOtherAgents() {
		ArrayList<AID> allR=new ArrayList<AID>();
		//Find all other agents and add them as receiver
        DFAgentDescription dfd = new DFAgentDescription();
        ServiceDescription sd = new ServiceDescription();
        sd.setType( "explorer" ); 
        dfd.addServices(sd);
        DFAgentDescription dfd2 = new DFAgentDescription();
        ServiceDescription sd1 = new ServiceDescription();
        sd1.setType( "collector" ); 
        dfd2.addServices(sd1);
        DFAgentDescription dfd3 = new DFAgentDescription();
        ServiceDescription sd2 = new ServiceDescription();
        sd2.setType( "silo" ); 
        dfd3.addServices(sd2);
        SearchConstraints ALL = new SearchConstraints();
        ALL.setMaxResults(new Long(-1));
        DFAgentDescription[] result;
        DFAgentDescription[] result2;
        DFAgentDescription[] result3;
		try {
			result = DFService.search(this, dfd,ALL);
			result2 = DFService.search(this, dfd2,ALL);
			result3 = DFService.search(this, dfd3,ALL);
			
			//every agents of every type is added to the receivers list (if different from sender)
			//For now, only explorer & collector.
			
			AID[] agents = new AID[result.length+result2.length+result3.length];
            for (int i=0; i<(result.length+result2.length+result3.length); i++) {
               
                if(i<result.length)
                		agents[i] = result[i].getName();
                else {
                		if(i<result.length+result2.length)
                			agents[i] = result2[i-result.length].getName() ;
                		else
                			agents[i] = result3[i-result.length-result2.length].getName() ;
                }
                
            		if (!getAID().equals(agents[i]) && !allR.contains(agents[i])) {
            			
            				allR.add(agents[i]);
            			
            		}
            		
            }
            
            
		} catch (FIPAException e1) {
			System.out.println("Error during DFS Search");
			e1.printStackTrace();
		}
		allOtherAgents=allR;
		
		
		return allR;
		
	}
	
	public boolean isEveryoneElseFinished() {
		return everyoneElseFinished;
	}
	

	public boolean setEveryoneElseFinished() {
		int s=allOtherAgents.size();
		if(s==0)
			return false;
		
		everyoneElseFinished= finishedAgents.size()==s;
		
		return everyoneElseFinished;
	}
	
	
	public void addNodeToGraph(Node n) {
		if(displayMyGraph)
			myGraph.addClosedNode(n);
	}
	
	//Verify if a node is opened by its name
	public boolean isOpen(String s) {
		return(getNodeById(s)==null);
	}
	
	/**
	 * Best node is the one with a big ammount of treasure and which is nearest
	 * We want also the quantity of treasure to be inferior to the free back pack space
	 * @return
	 */
	public String getBestNode() {
		String curr=getCurrentPosition();
		String myType = getMyTreasureType();
		int freeSpace= getBackPackFreeSpace();
		//System.out.println(getLocalName()+" "+freeSpace);
		
		dObj=new DjikstraObject(curr,myMap);
		
		String bestNode="";
		//Will contain the nodes adapted to the agent (with the matching type of treasure )
		ArrayList<String> bestNodes=new ArrayList<String>();
		int nbDiamonds,nbTreasure;
		for(Node n:myMap) {
			nbDiamonds=n.getDiamonds();
			nbTreasure=n.getTreasure();
			//If the node is interesting we add it
			if(myType.equals("Any")
					&& ((nbDiamonds>0 && nbDiamonds<=freeSpace )
							||
							(nbTreasure>0 && nbTreasure<=freeSpace ))) {
				bestNodes.add(n.getId());
				
			}
			else{
				if ((nbDiamonds>0 && nbDiamonds<=freeSpace
					&& myType.equals("Diamonds") ) 
					|| (nbTreasure>0 && nbTreasure<=freeSpace 
					&& myType.equals("Treasure") )) {
				
				bestNodes.add(n.getId());
				}
			}
		}
		//System.out.println(getLocalName()+" is choosing between these nodes"+bestNodes);
		int cout=Integer.MAX_VALUE;
		//Get the nearest one
		for (String s:bestNodes) {
			int size=dObj.getPath(s).size();
			if(size<cout) {
				bestNode=s;
				cout=size;
			}
		}		
		return bestNode;
	}
	
	/**
	 * Returns half of our map (to optimize the explo and share the map between two (or more) agents)
	 * @param current
	 * @return
	 */
	public ArrayList<String> getHalfOfMap(Node current){
		ArrayList<String> array=new ArrayList<String>();
		Random ran=new Random();
		int i;
		int size=myMap.size()/2;
		Node temp=current;
		while(array.size()<size) {
			array.add(temp.getId());
			i=ran.nextInt(temp.getNeighbours().size());
			temp=getNodeById(temp.getNeighbours().get(i));
		}
		return array;
	}
	
	/**
	 * Mise à jour d'un certain noeud dans notre map (complète)
	 * @param n
	 */
	public void setNode(Node n) {
		myMap.remove(getNodeById(n.getId()));
		myMap.add(n);
	}


	public enum agentType{
		COLLECTOR,
		EXPLORER,
		SILO,
		NONE
	}
	
	public agentType getType() {
		return type;
	}
	
	private CollectorDetails getCollectorDetails(AID a) {
		for(CollectorDetails cd:collectorsDetails) {
			if(cd.getAid().equals(a))
				return cd;
		}
		return null;
	}
	
	public void addMyDetails() {
		CollectorDetails myPreviousDetails=getCollectorDetails(getAID());
		if(myPreviousDetails!=null)
			collectorsDetails.remove(myPreviousDetails);
		//myPreviousDetails=new CollectorDetails(this.getAID(),this.getCurrentPosition(),this.maxCapacity/this.getBackPackFreeSpace());
		myPreviousDetails=new CollectorDetails(this.getAID(),this.getCurrentPosition(),this.getBackPackFreeSpace());
		collectorsDetails.add(myPreviousDetails);
	}

	public void updateCollectorDetails(ArrayList<CollectorDetails> newList) {
		
		CollectorDetails temp;
		for(CollectorDetails cd : newList) {
			temp=getCollectorDetails(cd.getAid());
			if(temp==null) {
				collectorsDetails.add(cd);
			}
			else {
				//Mon info est plus ancienne que la sienne
				if(temp.getDate()<cd.getDate()) {
					collectorsDetails.remove(temp);
					collectorsDetails.add(cd);
				}
			}
		}
		
		
		
		
	}

	/**
	 * Method only for the SILO (à déplacer dans tankerAgent)
	 * @return le meilleur des collecteurs à aller voir
	 */
	public String getBestCollectorPosition() {
		int capacite=Integer.MAX_VALUE;
		String bestDest="";
		for(CollectorDetails cd : collectorsDetails) {
			if(cd.getCapacity()<capacite) {
				capacite=cd.getCapacity();
				bestDest=cd.getPosition();
			}			
		}
		return bestDest;
	}

	
	
	
}
