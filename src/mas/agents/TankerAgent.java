package mas.agents;

import java.util.List;
import env.Attribute;
import env.Couple;
import env.EntityType;
import env.Environment;
import jade.core.behaviours.TickerBehaviour;
import mas.abstractAgent;

public class TankerAgent extends BasicAgent{

	/**
	 * Agent SILO (capacité de sac à dos illimité) - utilisé pour la décharge des trésors.
	 */
	private static final long serialVersionUID = -1784844593772918359L;

	protected void setup(){

		super.setup();
		registerOnDF("silo");

		//Add the behaviours
		//addBehaviour(new RandomTankerBehaviour(this));

	}

	/**
	 * This method is automatically called after doDelete()
	 */
	protected void takeDown(){

	}
}


/**************************************
 * 
 * 
 * 				BEHAVIOUR
 * 
 * 
 **************************************/

class RandomTankerBehaviour extends TickerBehaviour{
	/**
	 * When an agent choose to move
	 *  
	 */
	private static final long serialVersionUID = 9088209402507795289L;

	public RandomTankerBehaviour (final mas.abstractAgent myagent) {
		super(myagent, 10000);
		//super(myagent);
	}

	@Override
	public void onTick() {
		//Example to retrieve the current position
		String myPosition=((mas.abstractAgent)this.myAgent).getCurrentPosition();

		if (myPosition!=""){
			//List of observable from the agent's current position
			List<Couple<String,List<Attribute>>> lobs=((mas.abstractAgent)this.myAgent).observe();//myPosition
			//Se rapprocher de l'agent qui l'appelle ?
			//DO NOTHING. But it could be a good idea to change that
		}

	}

}