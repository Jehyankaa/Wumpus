package mas.agents;


import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import env.EntityType;
import env.Environment;
import jade.core.Profile;
import jade.core.ProfileImpl;
import jade.domain.DFService;
import jade.domain.FIPAException;
import jade.domain.FIPAAgentManagement.DFAgentDescription;
import jade.domain.FIPAAgentManagement.ServiceDescription;
import mas.abstractAgent;
import mas.behaviours.*;
import mas.data.Node;


public class ExploAgent extends BasicAgent{

	/**
	 * Agent explorateur (ne peut qu'observer)
	 */
	private static final long serialVersionUID = -1784844593772918359L;

	


	/**
	 * This method is automatically called when "agent".start() is executed.
	 * Consider that Agent is launched for the first time. 
	 * 			1) set the agent attributes 
	 *	 		2) add the behaviours
	 *          
	 */
	protected void setup(){

		super.setup();
		 
		//Register on the DF
		registerOnDF("explorer");

		//Add the behaviours
		//addBehaviour(new FirstExplorationBehaviour(this));
		

	}

	/**
	 * This method is automatically called after doDelete()
	 */
	protected void takeDown(){

	}
	 
	
	
			
		
}
