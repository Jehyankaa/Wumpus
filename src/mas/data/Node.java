package mas.data;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Objet noeud contenant les id des voisins, et informations sur trésors
 * @author Jehyanka
 *
 */
public class Node implements Serializable {
	
	
	private static final long serialVersionUID = 1L;
	private String id;
	private ArrayList<String> neighbours=new ArrayList<String>();
	//private NodeState state = NodeState.CLOSED;
	private int diamonds=0, treasure=0;
	
	
	
	public int getDiamonds() {
		return diamonds;
	}

	public void setDiamonds(int diamonds) {
		this.diamonds = diamonds;
	}

	public int getTreasure() {
		return treasure;
	}

	public void setTreasure(int treasure) {
		this.treasure = treasure;
	}

	@Override
	public String toString() {
		return ("Id : "+id + "\nNeighbours : \n"+neighbours+"\nDiamonds : "+diamonds+"	Treasure : "+treasure);
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public ArrayList<String> getNeighbours() {
		return neighbours;
	}

	public void setNeighbours(ArrayList<String> neighbours) {
		this.neighbours = neighbours;
	}

	public Node(String id, ArrayList<String> neighbours) {
		super();
		this.id = id;
		this.neighbours = neighbours;
	}
	
	
	
	/* Useless
	public NodeState getState() {
		return state;
	}

	public void setState(NodeState state) {
		this.state = state;
	}



	public enum NodeState {
		OPENED("opened",0),
		CLOSED("closed",1);
		private final String text;
		private final int number;

	    NodeState(final String text,final int number) {
	        this.text = text;
	        this.number=number;
	    }

	    public static NodeState fromString(String value) {
	        if (value != null) {
	            for (NodeState t : NodeState.values()) {
	                if (value.equalsIgnoreCase(t.text)) {
	                    return t;
	                }
	            }
	        }
	        return null;
	    }
	    
	    public static NodeState fromNumber(int value) {
	    		for (NodeState t : NodeState.values()) {
	    			if (value==(t.number)) {
	    				return t;
	    			}
	    		}

	    		return null;
	    }
	     @Override
	     public String toString() {
	         return this.text;
	     }
	}*/

}
