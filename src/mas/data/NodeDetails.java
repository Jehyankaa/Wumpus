package mas.data;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;

/**
 * For nodes with treasures on it.
 * Will be used to send new updates about interesting nodes
 * @author Jehyanka
 *
 */
public class NodeDetails implements Serializable {
	
	
	private static final long serialVersionUID = 1L;
	private String id;
	private long date;
	
	private int diamonds=0, treasure=0;
	
	
	
	public long getDate() {
		return date;
	}

	public void setDate() {
		this.date = new Date().getTime();
	}

	public int getDiamonds() {
		return diamonds;
	}

	public void setDiamonds(int diamonds) {
		this.diamonds = diamonds;
	}

	public int getTreasure() {
		return treasure;
	}

	public void setTreasure(int treasure) {
		this.treasure = treasure;
	}

	@Override
	public String toString() {
		return ("Id : "+id +"\nDiamonds : "+diamonds+"	Treasure : "+treasure+ "\nDate: "+date);
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}


	public NodeDetails(String id) {
		super();
		this.id = id;
	}
	
	

}
