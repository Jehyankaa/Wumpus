package mas.data;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;

/**
 * Objet contenant les infos suite à l'algorithme Djikstra à partir de la position courante.
 * Inspiré de https://fr.wikipedia.org/wiki/Algorithme_de_Dijkstra
 * @author Jehyanka
 *
 */

public class DjikstraObject implements Serializable {
	
	private static final long serialVersionUID = -5923964200721746414L;
	private HashMap<String,Integer> couts=new HashMap<String,Integer>();
	private HashMap<String,String> predecesseurs=new HashMap<String,String>();
	private String currentPosition="";
	private ArrayList<Node> map;
	
	
	
	public DjikstraObject(String currentPosition, ArrayList<Node> map) {
		
		super();	
		this.currentPosition = currentPosition;
		this.map = map;
		
		algoDjikstra();
	
	}
	
	

	public HashMap<String, Integer> getCouts() {
		return couts;
	}



	public void setCouts(HashMap<String, Integer> couts) {
		this.couts = couts;
	}



	public HashMap<String, String> getPredecesseurs() {
		return predecesseurs;
	}



	public void setPredecesseurs(HashMap<String, String> predecesseurs) {
		this.predecesseurs = predecesseurs;
	}


	//rend le chemin à l'envers
	public ArrayList<String> getPath (String destination){
		
		ArrayList<String> chemin= new ArrayList<String> ();
		String s = destination;
		while (!s.equals(currentPosition)) {
			chemin.add(s);
			s=this.predecesseurs.get(s);
		}
		
		return chemin;
	
	}
			
	
	public String findMin(ArrayList<Node> Q, HashMap<String,Integer> couts) {
		//Trouve min (Q)
		int min = Integer.MAX_VALUE;
		String sommet="";
		for(Node n:Q) {
			if(couts.get(n.getId())<min) {
				min=couts.get(n.getId());
				sommet=n.getId();
			}
		}
		return sommet;
	}
	
	
	public void algoDjikstra() {
		
		//Initialisation
		for(Node n : map) {
			couts.put(n.getId(), Integer.MAX_VALUE);
		}
		couts.put(currentPosition,0);
		
		ArrayList<Node> Q=new ArrayList<Node>(map);
		String s="";
		
		Node temp=null;
		while(!Q.isEmpty()) {
			s=findMin(Q,couts);
			
			for(Node n:map) {
				if (n.getId().equals(s))
					temp=n;
			}
			if (temp==null)
				System.out.println("Node "+s+" Non trouvée !");
			Q.remove(temp);
			
			//maj distances
			
			for (String s2:temp.getNeighbours()) {
				if(couts.get(s2)>(couts.get(s)+1)) {
					couts.put(s2,couts.get(s)+1);
					predecesseurs.put(s2,s);
				}
			}
			
			
		}
	}
}
