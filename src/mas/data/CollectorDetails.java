package mas.data;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;

import jade.core.AID;

/**
 * Object that contains a collector details usefull for the selection by SILO
 * @author Jehyanka
 *
 */
public class CollectorDetails implements Serializable {
	
	private static final long serialVersionUID = 1L;
	private AID aid;
	private String position;
	private int capacity;
	private long date;
	
	
	public CollectorDetails(AID aid, String position, int capacity) {
		super();
		this.aid = aid;
		this.position = position;
		this.capacity = capacity;
		setDate();
	}


	public AID getAid() {
		return aid;
	}

	public void setAid(AID aid) {
		this.aid = aid;
	}

	public String getPosition() {
		return position;
	}

	public void setPosition(String position) {
		this.position = position;
	}

	public int getCapacity() {
		return capacity;
	}



	public void setCapacity(int capacity) {
		this.capacity = capacity;
	}
	@Override
	public String toString() {
		return ("AID : "+aid +"\n position : "+position+"	capacity : "+capacity);
	}

	public long getDate() {
		return date;
	}




	public void setDate(long date) {
		this.date = date;
	}




	public void setDate() {
		this.date = new Date().getTime();
	}
	
	

}
